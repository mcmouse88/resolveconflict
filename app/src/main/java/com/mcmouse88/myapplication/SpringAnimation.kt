package com.mcmouse88.myapplication

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.view.View
import java.util.ArrayList

class SpringAnimation {

    companion object {

        fun init(view: View, amplitude: Float, duration: Int, delta: Int) {
            if (view.tag != null && view.tag as Boolean) return
            view.tag = true

            val items = ArrayList<Animator>()
            var transRight = ValueAnimator.ofFloat(0f, amplitude)
            transRight.addUpdateListener { animation ->
                view.translationX = animation.animatedValue as Float
            }
            items.add(transRight)

            var transLeft: ValueAnimator

            for (i in 0 until delta) {
                val temp1 = 1 - i.toFloat() / delta
                val temp2 = 1 - (i + 1).toFloat() / delta

                transLeft = ValueAnimator.ofFloat(amplitude * temp1, -amplitude * temp2)
                transLeft.addUpdateListener { animation ->
                    view.translationX = animation.animatedValue as Float
                }
                items.add(transLeft)

                transRight = ValueAnimator.ofFloat(-amplitude * temp2, amplitude * temp1)
                transRight.addUpdateListener { animation ->
                    view.translationX = animation.animatedValue as Float
                }
                items.add(transRight)
            }

            val transInitialPosition =
                ValueAnimator.ofFloat(amplitude * (1 - (delta - 1).toFloat() / delta), 0f)
            transInitialPosition.addUpdateListener { animation ->
                view.translationX = animation.animatedValue as Float
            }
            items.add(transInitialPosition)

            val animatorSet = AnimatorSet()
            animatorSet.playSequentially(items)
            animatorSet.duration = duration.toLong()
            animatorSet.start()

            animatorSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {}
                override fun onAnimationEnd(animator: Animator) {
                    view.tag = false
                }

                override fun onAnimationCancel(animator: Animator) {}
                override fun onAnimationRepeat(animator: Animator) {}
            })
        }

    }

}