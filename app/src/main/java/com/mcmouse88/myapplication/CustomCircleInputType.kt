package com.mcmouse88.myapplication

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import kotlin.math.max
import kotlin.math.min
import kotlin.properties.Delegates

class CustomCircleInputType(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int,
    defStyleRes: Int
) : View(context, attrs, defStyleAttr, defStyleRes) {

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : this(context, attrs, defStyleAttr, R.style.DefaultCustomCircleInputType)

    constructor(context: Context, attrs: AttributeSet?) : this(
        context,
        attrs,
        R.attr.customCircleInputTypeStyle
    )

    constructor(context: Context) : this(context, null)

    private var colorSchema: ColorSchema = ColorSchema.Normal
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
                invalidate()
            }
        }


    private var borderColor by Delegates.notNull<Int>()
    private var fillColor by Delegates.notNull<Int>()
    private var errorColor by Delegates.notNull<Int>()
    private var isEmptyItem by Delegates.notNull<Boolean>()
    private var itemCount by Delegates.notNull<Int>()

    private lateinit var borderColorPaint: Paint
    private lateinit var fillColorPaint: Paint
    private lateinit var errorColorPaint: Paint
    private lateinit var currentCellPaint: Paint
    private lateinit var gridPaint: Paint
    private val cellRect = RectF()

    private val fieldRect = RectF(0f, 0f, 0f, 0f)
    private var cellSize = 0f
    private var cellPadding = 0f

    private var currentFill = -1

    init {
        if (attrs != null) initAttributes(attrs, defStyleAttr, defStyleRes)
        else defaultAttributes()
        initPaints()
        isClickable = false
        isFocusable = false

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minWidth = suggestedMinimumWidth + paddingLeft + paddingRight
        val minHeight = suggestedMinimumHeight + paddingTop + paddingBottom

        val cellSizeInPixels = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            DESIRED_CELL_SIZE,
            resources.displayMetrics
        ).toInt()

        val desiredWidth = max(minWidth, itemCount * cellSizeInPixels + paddingLeft + paddingRight)
        val desiredHeight =
            max(minHeight, CONST_COLUMN * cellSizeInPixels + paddingTop + paddingBottom)

        setMeasuredDimension(
            resolveSize(desiredWidth, widthMeasureSpec),
            resolveSize(desiredHeight, heightMeasureSpec)
        )
    }

    override fun onSizeChanged(width: Int, height: Int, oldwidth: Int, oldheight: Int) {
        super.onSizeChanged(width, height, oldwidth, oldheight)
        updateViewSize()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawGrid(canvas)
        drawCells(canvas)
    }

    fun add() {
        if (currentFill != itemCount - 1) {
            currentFill++
            colorSchema = ColorSchema.Fill
        }
    }

    fun remove() {
        if (currentFill != -1) currentFill--
        else colorSchema = ColorSchema.Normal
    }

    fun reset() {
        currentFill = -1
        colorSchema = ColorSchema.Normal
    }

    fun setError() {
        colorSchema = ColorSchema.Error
    }

    private fun changeFillItems(canvas: Canvas) {
        for (i in 0..currentFill) {
            drawBFilledItem(canvas, i, 0)
        }
        invalidate()
    }

    private fun initAttributes(
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.CustomCircleInputType,
            defStyleAttr,
            defStyleRes
        )

        borderColor =
            typedArray.getColor(R.styleable.CustomCircleInputType_borderColor, BORDER_DEFAULT_COLOR)
        fillColor =
            typedArray.getColor(R.styleable.CustomCircleInputType_fillColor, FILL_DEFAULT_COLOR)
        errorColor =
            typedArray.getColor(R.styleable.CustomCircleInputType_errorColor, ERROR_DEFAULT_COLOR)

        isEmptyItem = typedArray.getBoolean(
            R.styleable.CustomCircleInputType_isEmptyItem,
            IS_EMPTY_ITEM_DEFAULT
        )

        val userItemCount =
            typedArray.getInt(R.styleable.CustomCircleInputType_itemCount, MIN_ITEM_COUNT)
        itemCount =
            if (userItemCount in MIN_ITEM_COUNT..MAX_ITEM_COUNT) userItemCount else MIN_ITEM_COUNT
        typedArray.recycle()
    }

    private fun defaultAttributes() {
        borderColor = BORDER_DEFAULT_COLOR
        fillColor = FILL_DEFAULT_COLOR
        errorColor = ERROR_DEFAULT_COLOR
        isEmptyItem = IS_EMPTY_ITEM_DEFAULT
        itemCount = 4
    }

    private fun initPaints() {
        borderColorPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        borderColorPaint.color = borderColor
        borderColorPaint.style = Paint.Style.STROKE
        borderColorPaint.strokeWidth = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            3f,
            resources.displayMetrics
        )

        fillColorPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        fillColorPaint.color = fillColor
        fillColorPaint.style = Paint.Style.FILL

        errorColorPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        errorColorPaint.color = errorColor
        errorColorPaint.style = Paint.Style.FILL

        currentCellPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        currentCellPaint.color = Color.argb(0, 255, 255, 255)
        currentCellPaint.style = Paint.Style.FILL

        gridPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        gridPaint.color = Color.argb(0, 255, 255, 255)
        gridPaint.style = Paint.Style.FILL
    }

    private fun updateViewSize() {
        val safeWidth = width - paddingLeft - paddingRight
        val safeHeight = height - paddingTop - paddingBottom

        val cellWidth = safeWidth / itemCount.toFloat()
        val cellHeight = safeHeight / CONST_COLUMN.toFloat()

        cellSize = min(cellWidth, cellHeight)
        cellPadding = cellSize * 0.2f

        val fieldWidth = cellSize * itemCount
        val fieldHeight = cellSize * CONST_COLUMN

        fieldRect.left = paddingLeft + (safeWidth - fieldWidth) / 2
        fieldRect.top = paddingTop + (safeHeight - fieldHeight) / 2
        fieldRect.right = fieldRect.left + fieldWidth
        fieldRect.bottom = fieldRect.top + fieldHeight
    }

    private fun drawGrid(canvas: Canvas) {
        val xStart = fieldRect.left
        val xEnd = fieldRect.right
        for (i in 0..CONST_COLUMN) {
            val y = fieldRect.top + cellSize * i
            canvas.drawLine(xStart, y, xEnd, y, gridPaint)
        }

        val yStart = fieldRect.top
        val yEnd = fieldRect.bottom
        for (i in 0..itemCount) {
            val x = fieldRect.left + cellSize * i
            canvas.drawLine(x, yStart, x, yEnd, gridPaint)
        }
    }

    private fun drawCells(canvas: Canvas) {
        for (row in 0 until itemCount) {
            for (column in 0 until CONST_COLUMN) {
                when (colorSchema) {
                    is ColorSchema.Error -> drawErrorItem(canvas, row, column)
                    is ColorSchema.Normal -> drawBorderItem(canvas, row, column)
                    is ColorSchema.Fill -> {
                        drawBorderItem(canvas, row, column)
                        changeFillItems(canvas)
                    }
                }
            }
        }
    }

    private fun drawBorderItem(canvas: Canvas, row: Int, column: Int) {
        val cell = getCellRect(row, column)
        canvas.drawCircle(cell.centerX(), cell.centerY(), cell.width() / 2, borderColorPaint)
    }

    private fun drawBFilledItem(canvas: Canvas, row: Int, column: Int) {
        val cell = getCellRect(row, column)
        canvas.drawCircle(cell.centerX(), cell.centerY(), cell.width() / 2, fillColorPaint)
    }

    private fun drawErrorItem(canvas: Canvas, row: Int, column: Int) {
        val cell = getCellRect(row, column)
        canvas.drawCircle(cell.centerX(), cell.centerY(), cell.width() / 2, errorColorPaint)
    }

    private fun getCellRect(row: Int, column: Int): RectF {
        cellRect.left = fieldRect.left + row * cellSize + cellPadding
        cellRect.top = fieldRect.top + column * cellSize + cellPadding
        cellRect.right = cellRect.left + cellSize - cellPadding * 2
        cellRect.bottom = cellRect.top + cellSize - cellPadding * 2
        return cellRect
    }

    sealed class ColorSchema {
        object Normal : ColorSchema()
        object Fill : ColorSchema()
        object Error : ColorSchema()
    }

    companion object {
        private const val BORDER_DEFAULT_COLOR = Color.BLUE
        private const val FILL_DEFAULT_COLOR = Color.LTGRAY
        private const val ERROR_DEFAULT_COLOR = Color.RED
        private const val IS_EMPTY_ITEM_DEFAULT = false

        private const val CONST_COLUMN = 1
        private const val DESIRED_CELL_SIZE = 24F
        private const val MIN_ITEM_COUNT = 4
        private const val MAX_ITEM_COUNT = 8
    }
}