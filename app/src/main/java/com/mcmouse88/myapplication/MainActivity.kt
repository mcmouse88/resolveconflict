package com.mcmouse88.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mcmouse88.myapplication.CustomCircleInputType.ColorSchema
import com.mcmouse88.myapplication.CustomCircleInputType.ColorSchema.Normal
import com.mcmouse88.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        var isError = false
    }
}